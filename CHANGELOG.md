# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## Unreleased
### Added
* Add new folder "assets" to store all assets files (css, js, etc.).
* Add CSS framework

## [0.2.0-alpha](https://gitlab.com/SmashScharrer-Dev/demyqewiki-gg-nuxt/-/compare/v0.1.3-alpha...v0.2.0-alpha) - 2022-08-02
### Fixed
* Fix bug in Linkedin link.
### Added
* Add new folder "pages" to enable the Nuxt router feature.
* Add Demyqewiki.gg main page.

## [0.1.3-alpha](https://gitlab.com/SmashScharrer-Dev/demyqewiki-gg-nuxt/-/compare/v0.1.2-alpha...v0.1.3-alpha) - 2022-08-02
### Changed
* Update README file with [readme-md-generator](https://github.com/kefranabg/readme-md-generator) NPM package.

## [0.1.2-alpha](https://gitlab.com/SmashScharrer-Dev/demyqewiki-gg-nuxt/-/compare/v0.1.1-alpha...v0.1.2-alpha) - 2022-08-02
### Added
*  Add CHANGELOG.md file.

## [0.1.1-alpha](https://gitlab.com/SmashScharrer-Dev/demyqewiki-gg-nuxt/-/compare/v0.1.0-alpha...v0.1.1-alpha) - 2022-08-02
### Added
*  Add LICENCE.md file.

## [0.1.0-alpha](https://gitlab.com/SmashScharrer-Dev/demyqewiki-gg-nuxt/-/releases/v0.1.0-alpha) - 2022-08-02
### Added
*  Generate new Nuxt3 project.

