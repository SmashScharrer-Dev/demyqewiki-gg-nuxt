<h1 align="center">Welcome to Demyqewiki.gg 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.2.0--alpha-blue.svg?cacheSeconds=2592000" />
  <img src="https://img.shields.io/badge/npm-%3E%3D8.5.5-blue.svg" />
  <img src="https://img.shields.io/badge/node-%3E%3D16.15.0-blue.svg" />
  <a href="https://gitlab.com/SmashScharrer-Dev/demyqewiki-gg-nuxt/-/blob/main/LICENSE" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> League of Legends players statistics website with Nuxt.

### 🏠 [Homepage](https://gitlab.com/SmashScharrer-Dev/demyqewiki-gg-nuxt)

### ✨ [Demo](https://demyqewiki-gg-nuxt3.vercel.app)

## Setup
### Prerequisites
- npm >=8.5.5
- node >=16.15.0
### Nuxt 3 documentation
Look at the [nuxt 3 documentation](https://v3.nuxtjs.org) to learn more.
### Install the dependencies
```sh
npm install
```
### Start the development server
```bash
npm run dev
```
The development server start on http://localhost:3000
### Preview before build
```bash
npm run preview
```
Locally preview production build.
### Build the application for production
```bash
npm run build
```
Checkout the [deployment documentation](https://v3.nuxtjs.org/guide/deploy/presets) for more information.

## Author
👤 **SmashScharrer**
* Github (rarely used) : [@SmashScharrer](https://github.com/SmashScharrer)
* Gitlab : [@SmashScharrer-Dev](https://gitlab.com/SmashScharrer-Dev)
* LinkedIn : [@Nathan GUIRADO-PATRICO](https://www.linkedin.com/in/nathan-guirado-patrico)

## 🤝 Contributing
Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitlab.com/SmashScharrer-Dev/demyqewiki-gg-nuxt/-/issues). You can also take a look at the [contributing guide](https://gitlab.com/SmashScharrer-Dev/demyqewiki-gg-nuxt/blob/master/CONTRIBUTING.md).

## Show your support
Give a ⭐️ if this project helped you!

## 📝 License
Copyright © 2022 [SmashScharrer-Dev](https://gitlab.com/SmashScharrer-Dev).<br />
This project is [MIT](https://gitlab.com/SmashScharrer-Dev/demyqewiki-gg-nuxt/-/blob/main/LICENSE) licensed.

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
